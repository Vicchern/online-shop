<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextRoot" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Online Shop - ${title}</title>s

    <!-- Bootstrap core CSS -->
    <link href="<c:url value="/resources/css/bootstrap.min.css" />"
          rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap-lux-theme.css" />"
          rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<c:url value = "/resources/css/shop-homepage.css"/>" rel="stylesheet">



</head>

<body>

<div class="wrapper">
    <!-- Navigation -->
    <%@include file="shared/navBar.jsp" %>

        <c:if test="${userClickMainContent == true}">
            <%@include file="main-content.jsp" %>
        </c:if>

        <c:if test="${userClickAbout == true}">
            <%@include file="about.jsp" %>
        </c:if>

        <c:if test="${userClickContacts == true}">
            <%@include file="contacts.jsp" %>
        </c:if>

        <c:if test="${userClickProducts == true}">
            <%@include file="products.jsp" %>
        </c:if>

    <!-- Footer -->
    <%@include file="shared/footer.jsp" %>

    <!-- Bootstrap core JavaScript -->
    <script src="<c:url value="/resources/js/jquery.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.bundle.min.js"/>"></script>
    <script src="<c:url value="/resources/js/myApp.js"/>"></script>
</div>

</body>

</html>
