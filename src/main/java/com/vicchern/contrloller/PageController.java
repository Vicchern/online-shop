package com.vicchern.contrloller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PageController {

    @GetMapping(value = {"/", "/home"})
    public ModelAndView getHomePage() {

        ModelAndView modelAndView = new ModelAndView("home");

        modelAndView.addObject("title", "Home");
        modelAndView.addObject("userClickMainContent",true);
        return modelAndView;
    }

    @GetMapping(value = {"/about"})
    public ModelAndView getAboutPage() {

        ModelAndView modelAndView = new ModelAndView("home");

        modelAndView.addObject("title", "About Us");
        modelAndView.addObject("userClickAbout",true);
        return modelAndView;
    }

    @GetMapping(value = {"/contacts"})
    public ModelAndView getContactsPage() {

        ModelAndView modelAndView = new ModelAndView("home");

        modelAndView.addObject("title", "Contact Us");
        modelAndView.addObject("userClickContacts",true);
        return modelAndView;
    }

    @GetMapping(value = {"/products"})
    public ModelAndView getProductsPage() {

        ModelAndView modelAndView = new ModelAndView("home");

        modelAndView.addObject("title", "Products");
        modelAndView.addObject("userClickProducts",true);

        return modelAndView;
    }

}
