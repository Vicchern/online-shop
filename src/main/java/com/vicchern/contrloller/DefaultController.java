package com.vicchern.contrloller;

//import com.sun.org.slf4j.internal.Logger;
//import com.sun.org.slf4j.internal.LoggerFactory;
import com.vicchern.dto.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

public class DefaultController {
// private final static Logger logger = LoggerFactory.getLogger(DefaultController.class);

    /**
     * Handles all the exception which were not handled locally
     *
     * @param ex the exception which occurred anywhere in the code
     * @return the error message as a JSON
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity defaultExceptionHandler(Exception ex) {
//        logger.error("An error occurred:", ex);

        ErrorDto errorDto = new ErrorDto("Something went wrong", ex.getMessage());

//        logger.warn("Sending the error message to the client");
        return ResponseEntity
                .status(HttpStatus.I_AM_A_TEAPOT)
                .body(errorDto);
    }
}
